# Game of Life

This is an application written using C# and [Windows Forms](https://en.wikipedia.org/wiki/Windows_Forms) which simulates *[Conway's Game of Life](https://en.wikipedia.org/wiki/Conway's_Game_of_Life)*. Additionally, a built-in arcade game of [snake](https://en.wikipedia.org/wiki/Snake_(video_game_genre)) is included as an [Easter egg](https://en.wikipedia.org/wiki/Easter_egg_(media)#Software).

The *Game of Life* is a [cellular automaton](https://en.wikipedia.org/wiki/Cellular_automaton) (a [model of computation](https://en.wikipedia.org/wiki/Model_of_computation)) which makes fundamental aspects of [automata theory](https://en.wikipedia.org/wiki/Automata_theory) tangible and demonstrates the nature of [Turing completeness](https://en.wikipedia.org/wiki/Turing_completeness) within the context of a visualization of transitions between states of cells in a grid based on simple rules.

*Simulation of a [Gosper glider gun](https://conwaylife.com/wiki/Gosper_glider_gun), a finite pattern that loops infinitely:*

![](Recordings/Glider.mp4)

## Features

- Customizable grid layout and size
- Two boundary types, finite and toroidal (wrapping)
- Adjustable simulation speed and step-by-step simulation
- Fully customizable colors for the game of life and snake
- [Pseudorandom generation](https://en.wikipedia.org/wiki/Pseudorandom_number_generator) of worlds based on custom [seeds](https://en.wikipedia.org/wiki/Random_seed)
- Exporting/importing of [serialized](https://en.wikipedia.org/wiki/Serialization) world files
- A built-in arcade game of snake

*Random generation of worlds using the current time as a seed:*

![](Recordings/Random.mp4)

*Snake Easter egg! 🐍*

![](Recordings/Snake.mp4)

## Releases

Compiled binaries for Windows are available [here](https://gitlab.com/psahani/game-of-life/-/releases/). Note that the *[Century Gothic](https://en.wikipedia.org/wiki/Century_Gothic)* font must be installed for the display of neighbor counts to function properly.