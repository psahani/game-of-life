﻿namespace Game_of_Life {
    partial class OptionsDialog {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonApply = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageWorld = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkResizable = new System.Windows.Forms.CheckBox();
            this.numericCellSize = new System.Windows.Forms.NumericUpDown();
            this.labelCellSize = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numericWidth = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.numericHeight = new System.Windows.Forms.NumericUpDown();
            this.numericGridThickness = new System.Windows.Forms.NumericUpDown();
            this.tabPageGame = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoundary = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.graphicsPanel = new Game_of_Life.GraphicsPanel();
            this.numericFPS = new System.Windows.Forms.NumericUpDown();
            this.radioMilliseconds = new System.Windows.Forms.RadioButton();
            this.radioFPS = new System.Windows.Forms.RadioButton();
            this.tabPageColors = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonTextColor = new System.Windows.Forms.Button();
            this.buttonGridColor = new System.Windows.Forms.Button();
            this.buttonBackgroundColor = new System.Windows.Forms.Button();
            this.buttonCellColor = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.numericMilliseconds = new System.Windows.Forms.NumericUpDown();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.tabControl1.SuspendLayout();
            this.tabPageWorld.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericCellSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericGridThickness)).BeginInit();
            this.tabPageGame.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericFPS)).BeginInit();
            this.tabPageColors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericMilliseconds)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(138, 266);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(219, 266);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonReset
            // 
            this.buttonReset.Enabled = false;
            this.buttonReset.Location = new System.Drawing.Point(12, 266);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(120, 23);
            this.buttonReset.TabIndex = 2;
            this.buttonReset.Text = "Reset to Defaults";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // buttonApply
            // 
            this.buttonApply.Enabled = false;
            this.buttonApply.Location = new System.Drawing.Point(300, 266);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(75, 23);
            this.buttonApply.TabIndex = 3;
            this.buttonApply.Text = "Apply";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageWorld);
            this.tabControl1.Controls.Add(this.tabPageGame);
            this.tabControl1.Controls.Add(this.tabPageColors);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(385, 255);
            this.tabControl1.TabIndex = 4;
            // 
            // tabPageWorld
            // 
            this.tabPageWorld.Controls.Add(this.groupBox1);
            this.tabPageWorld.Controls.Add(this.label9);
            this.tabPageWorld.Controls.Add(this.label7);
            this.tabPageWorld.Controls.Add(this.numericWidth);
            this.tabPageWorld.Controls.Add(this.label8);
            this.tabPageWorld.Controls.Add(this.numericHeight);
            this.tabPageWorld.Controls.Add(this.numericGridThickness);
            this.tabPageWorld.Location = new System.Drawing.Point(4, 22);
            this.tabPageWorld.Name = "tabPageWorld";
            this.tabPageWorld.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageWorld.Size = new System.Drawing.Size(377, 229);
            this.tabPageWorld.TabIndex = 1;
            this.tabPageWorld.Text = "World";
            this.tabPageWorld.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkResizable);
            this.groupBox1.Controls.Add(this.numericCellSize);
            this.groupBox1.Controls.Add(this.labelCellSize);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(365, 65);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Window";
            // 
            // checkResizable
            // 
            this.checkResizable.AutoSize = true;
            this.checkResizable.Checked = true;
            this.checkResizable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkResizable.Location = new System.Drawing.Point(20, 25);
            this.checkResizable.Name = "checkResizable";
            this.checkResizable.Size = new System.Drawing.Size(72, 17);
            this.checkResizable.TabIndex = 5;
            this.checkResizable.Text = "Resizable";
            this.checkResizable.UseVisualStyleBackColor = true;
            this.checkResizable.CheckedChanged += new System.EventHandler(this.checkResizable_CheckedChanged);
            // 
            // numericCellSize
            // 
            this.numericCellSize.Enabled = false;
            this.numericCellSize.Location = new System.Drawing.Point(200, 24);
            this.numericCellSize.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericCellSize.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericCellSize.Name = "numericCellSize";
            this.numericCellSize.Size = new System.Drawing.Size(150, 20);
            this.numericCellSize.TabIndex = 1;
            this.numericCellSize.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // labelCellSize
            // 
            this.labelCellSize.AutoSize = true;
            this.labelCellSize.Enabled = false;
            this.labelCellSize.Location = new System.Drawing.Point(109, 26);
            this.labelCellSize.Name = "labelCellSize";
            this.labelCellSize.Size = new System.Drawing.Size(85, 13);
            this.labelCellSize.TabIndex = 5;
            this.labelCellSize.Text = "Cell Size (pixels):";
            this.labelCellSize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 188);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(116, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Grid Thickness (pixels):";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Grid Width (cells):";
            // 
            // numericWidth
            // 
            this.numericWidth.Location = new System.Drawing.Point(156, 90);
            this.numericWidth.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericWidth.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericWidth.Name = "numericWidth";
            this.numericWidth.Size = new System.Drawing.Size(200, 20);
            this.numericWidth.TabIndex = 2;
            this.numericWidth.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 140);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Grid Height (cells):";
            // 
            // numericHeight
            // 
            this.numericHeight.Location = new System.Drawing.Point(156, 138);
            this.numericHeight.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericHeight.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericHeight.Name = "numericHeight";
            this.numericHeight.Size = new System.Drawing.Size(200, 20);
            this.numericHeight.TabIndex = 3;
            this.numericHeight.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // numericGridThickness
            // 
            this.numericGridThickness.Location = new System.Drawing.Point(156, 186);
            this.numericGridThickness.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericGridThickness.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericGridThickness.Name = "numericGridThickness";
            this.numericGridThickness.Size = new System.Drawing.Size(200, 20);
            this.numericGridThickness.TabIndex = 0;
            this.numericGridThickness.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tabPageGame
            // 
            this.tabPageGame.Controls.Add(this.label5);
            this.tabPageGame.Controls.Add(this.comboBoundary);
            this.tabPageGame.Controls.Add(this.groupBox2);
            this.tabPageGame.Location = new System.Drawing.Point(4, 22);
            this.tabPageGame.Name = "tabPageGame";
            this.tabPageGame.Size = new System.Drawing.Size(377, 229);
            this.tabPageGame.TabIndex = 2;
            this.tabPageGame.Text = "Game";
            this.tabPageGame.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Boundary Type:";
            // 
            // comboBoundary
            // 
            this.comboBoundary.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoundary.FormattingEnabled = true;
            this.comboBoundary.Items.AddRange(new object[] {
            "Toroidal",
            "Finite"});
            this.comboBoundary.Location = new System.Drawing.Point(165, 190);
            this.comboBoundary.Name = "comboBoundary";
            this.comboBoundary.Size = new System.Drawing.Size(200, 21);
            this.comboBoundary.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numericMilliseconds);
            this.groupBox2.Controls.Add(this.graphicsPanel);
            this.groupBox2.Controls.Add(this.numericFPS);
            this.groupBox2.Controls.Add(this.radioMilliseconds);
            this.groupBox2.Controls.Add(this.radioFPS);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(365, 170);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Speed";
            // 
            // graphicsPanel
            // 
            this.graphicsPanel.Location = new System.Drawing.Point(13, 115);
            this.graphicsPanel.Name = "graphicsPanel";
            this.graphicsPanel.Size = new System.Drawing.Size(340, 40);
            this.graphicsPanel.TabIndex = 3;
            this.graphicsPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.graphicsPanel1_Paint);
            // 
            // numericFPS
            // 
            this.numericFPS.Location = new System.Drawing.Point(159, 30);
            this.numericFPS.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericFPS.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericFPS.Name = "numericFPS";
            this.numericFPS.Size = new System.Drawing.Size(200, 20);
            this.numericFPS.TabIndex = 2;
            this.numericFPS.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericFPS.ValueChanged += new System.EventHandler(this.numericFPS_ValueChanged);
            // 
            // radioMilliseconds
            // 
            this.radioMilliseconds.AutoSize = true;
            this.radioMilliseconds.Location = new System.Drawing.Point(20, 70);
            this.radioMilliseconds.Name = "radioMilliseconds";
            this.radioMilliseconds.Size = new System.Drawing.Size(82, 17);
            this.radioMilliseconds.TabIndex = 1;
            this.radioMilliseconds.Text = "Milliseconds";
            this.radioMilliseconds.UseVisualStyleBackColor = true;
            // 
            // radioFPS
            // 
            this.radioFPS.AutoSize = true;
            this.radioFPS.Checked = true;
            this.radioFPS.Location = new System.Drawing.Point(20, 30);
            this.radioFPS.Name = "radioFPS";
            this.radioFPS.Size = new System.Drawing.Size(45, 17);
            this.radioFPS.TabIndex = 0;
            this.radioFPS.TabStop = true;
            this.radioFPS.Text = "FPS";
            this.radioFPS.UseVisualStyleBackColor = true;
            this.radioFPS.CheckedChanged += new System.EventHandler(this.radioFPS_CheckedChanged);
            // 
            // tabPageColors
            // 
            this.tabPageColors.Controls.Add(this.label4);
            this.tabPageColors.Controls.Add(this.label3);
            this.tabPageColors.Controls.Add(this.label2);
            this.tabPageColors.Controls.Add(this.label1);
            this.tabPageColors.Controls.Add(this.buttonTextColor);
            this.tabPageColors.Controls.Add(this.buttonGridColor);
            this.tabPageColors.Controls.Add(this.buttonBackgroundColor);
            this.tabPageColors.Controls.Add(this.buttonCellColor);
            this.tabPageColors.Location = new System.Drawing.Point(4, 22);
            this.tabPageColors.Name = "tabPageColors";
            this.tabPageColors.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageColors.Size = new System.Drawing.Size(377, 229);
            this.tabPageColors.TabIndex = 0;
            this.tabPageColors.Text = "Colors";
            this.tabPageColors.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 193);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Neighbor Count:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Grid:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Background:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Cell:";
            // 
            // buttonTextColor
            // 
            this.buttonTextColor.Location = new System.Drawing.Point(160, 184);
            this.buttonTextColor.Name = "buttonTextColor";
            this.buttonTextColor.Size = new System.Drawing.Size(200, 30);
            this.buttonTextColor.TabIndex = 3;
            this.buttonTextColor.UseVisualStyleBackColor = true;
            this.buttonTextColor.Click += new System.EventHandler(this.buttonTextColor_Click);
            // 
            // buttonGridColor
            // 
            this.buttonGridColor.Location = new System.Drawing.Point(160, 128);
            this.buttonGridColor.Name = "buttonGridColor";
            this.buttonGridColor.Size = new System.Drawing.Size(200, 30);
            this.buttonGridColor.TabIndex = 2;
            this.buttonGridColor.UseVisualStyleBackColor = true;
            this.buttonGridColor.Click += new System.EventHandler(this.buttonGridColor_Click);
            // 
            // buttonBackgroundColor
            // 
            this.buttonBackgroundColor.Location = new System.Drawing.Point(160, 72);
            this.buttonBackgroundColor.Name = "buttonBackgroundColor";
            this.buttonBackgroundColor.Size = new System.Drawing.Size(200, 30);
            this.buttonBackgroundColor.TabIndex = 1;
            this.buttonBackgroundColor.UseVisualStyleBackColor = true;
            this.buttonBackgroundColor.Click += new System.EventHandler(this.buttonBackgroundColor_Click);
            // 
            // buttonCellColor
            // 
            this.buttonCellColor.BackColor = System.Drawing.Color.Transparent;
            this.buttonCellColor.Location = new System.Drawing.Point(160, 16);
            this.buttonCellColor.Name = "buttonCellColor";
            this.buttonCellColor.Size = new System.Drawing.Size(200, 30);
            this.buttonCellColor.TabIndex = 0;
            this.buttonCellColor.UseVisualStyleBackColor = false;
            this.buttonCellColor.Click += new System.EventHandler(this.buttonCellColor_Click);
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // numericMilliseconds
            // 
            this.numericMilliseconds.Enabled = false;
            this.numericMilliseconds.Location = new System.Drawing.Point(159, 70);
            this.numericMilliseconds.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericMilliseconds.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericMilliseconds.Name = "numericMilliseconds";
            this.numericMilliseconds.Size = new System.Drawing.Size(200, 20);
            this.numericMilliseconds.TabIndex = 4;
            this.numericMilliseconds.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericMilliseconds.ValueChanged += new System.EventHandler(this.numericMilliseconds_ValueChanged);
            // 
            // OptionsDialog
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(384, 301);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OptionsDialog";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Options";
            this.tabControl1.ResumeLayout(false);
            this.tabPageWorld.ResumeLayout(false);
            this.tabPageWorld.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericCellSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericGridThickness)).EndInit();
            this.tabPageGame.ResumeLayout(false);
            this.tabPageGame.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericFPS)).EndInit();
            this.tabPageColors.ResumeLayout(false);
            this.tabPageColors.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericMilliseconds)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageColors;
        private System.Windows.Forms.TabPage tabPageWorld;
        private System.Windows.Forms.TabPage tabPageGame;
        private System.Windows.Forms.Button buttonCellColor;
        private System.Windows.Forms.Button buttonTextColor;
        private System.Windows.Forms.Button buttonGridColor;
        private System.Windows.Forms.Button buttonBackgroundColor;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown numericWidth;
        private System.Windows.Forms.NumericUpDown numericCellSize;
        private System.Windows.Forms.NumericUpDown numericGridThickness;
        private System.Windows.Forms.NumericUpDown numericHeight;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoundary;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton radioMilliseconds;
        private System.Windows.Forms.RadioButton radioFPS;
        private System.Windows.Forms.CheckBox checkResizable;
        private System.Windows.Forms.NumericUpDown numericFPS;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelCellSize;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private GraphicsPanel graphicsPanel;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.NumericUpDown numericMilliseconds;
        private System.Windows.Forms.ColorDialog colorDialog;
    }
}