﻿namespace Game_of_Life {
    class Cell {
        //True if alive, False if dead
        public bool state { get; set; }
        //Number of generations the cell has been alive
        public int generation { get; set; }

        public Cell() {
            this.state = false;
            this.generation = 0;
        }
    }
}
