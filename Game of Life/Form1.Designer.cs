﻿namespace Game_of_Life {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.open = new System.Windows.Forms.ToolStripMenuItem();
            this.save = new System.Windows.Forms.ToolStripMenuItem();
            this.import = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.quit = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grid = new System.Windows.Forms.ToolStripMenuItem();
            this.neighborCount = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.options = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.about = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.clear = new System.Windows.Forms.ToolStripButton();
            this.reset = new System.Windows.Forms.ToolStripButton();
            this.randomize = new System.Windows.Forms.ToolStripSplitButton();
            this.randomizeTime = new System.Windows.Forms.ToolStripMenuItem();
            this.randomizeCurrentSeed = new System.Windows.Forms.ToolStripMenuItem();
            this.randomizeNewSeed = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.run = new System.Windows.Forms.ToolStripSplitButton();
            this.runTo = new System.Windows.Forms.ToolStripMenuItem();
            this.pause = new System.Windows.Forms.ToolStripButton();
            this.next = new System.Windows.Forms.ToolStripButton();
            this.snake = new System.Windows.Forms.ToolStripSplitButton();
            this.pauseGame = new System.Windows.Forms.ToolStripMenuItem();
            this.setSnakeColor = new System.Windows.Forms.ToolStripMenuItem();
            this.setPowerUpColor = new System.Windows.Forms.ToolStripMenuItem();
            this.resetDefaultColors = new System.Windows.Forms.ToolStripMenuItem();
            this.exitGame = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.info = new System.Windows.Forms.ToolStripStatusLabel();
            this.graphicsPanel = new Game_of_Life.GraphicsPanel();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.importFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graphicsPanelSnake = new Game_of_Life.GraphicsPanel();
            this.gameTimer = new System.Windows.Forms.Timer(this.components);
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.menuStrip.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(526, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            this.menuStrip.MenuDeactivate += new System.EventHandler(this.menuStrip1_MenuDeactivate);
            this.menuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.open,
            this.save,
            this.import,
            this.toolStripSeparator2,
            this.quit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // open
            // 
            this.open.Image = global::Game_of_Life.Properties.Resources.open;
            this.open.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.open.Name = "open";
            this.open.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.open.Size = new System.Drawing.Size(147, 22);
            this.open.Text = "&Open";
            this.open.Click += new System.EventHandler(this.open_Click);
            // 
            // save
            // 
            this.save.Image = global::Game_of_Life.Properties.Resources.save;
            this.save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.save.Name = "save";
            this.save.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.save.Size = new System.Drawing.Size(147, 22);
            this.save.Text = "&Save";
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // import
            // 
            this.import.Image = global::Game_of_Life.Properties.Resources.import;
            this.import.Name = "import";
            this.import.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.import.Size = new System.Drawing.Size(147, 22);
            this.import.Text = "&Import";
            this.import.Click += new System.EventHandler(this.import_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(144, 6);
            // 
            // quit
            // 
            this.quit.Image = global::Game_of_Life.Properties.Resources.quit;
            this.quit.Name = "quit";
            this.quit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.quit.Size = new System.Drawing.Size(147, 22);
            this.quit.Text = "&Quit";
            this.quit.Click += new System.EventHandler(this.quit_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.grid,
            this.neighborCount});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // grid
            // 
            this.grid.Checked = true;
            this.grid.CheckOnClick = true;
            this.grid.CheckState = System.Windows.Forms.CheckState.Checked;
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(160, 22);
            this.grid.Text = "Grid";
            // 
            // neighborCount
            // 
            this.neighborCount.CheckOnClick = true;
            this.neighborCount.Name = "neighborCount";
            this.neighborCount.Size = new System.Drawing.Size(160, 22);
            this.neighborCount.Text = "Neighbor Count";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.options});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // options
            // 
            this.options.Image = global::Game_of_Life.Properties.Resources.options;
            this.options.Name = "options";
            this.options.Size = new System.Drawing.Size(125, 22);
            this.options.Text = "Options...";
            this.options.Click += new System.EventHandler(this.options_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.about});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // about
            // 
            this.about.Image = global::Game_of_Life.Properties.Resources.about;
            this.about.Name = "about";
            this.about.Size = new System.Drawing.Size(177, 22);
            this.about.Text = "About Game of Life";
            this.about.Click += new System.EventHandler(this.about_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clear,
            this.reset,
            this.randomize,
            this.toolStripSeparator1,
            this.run,
            this.pause,
            this.next,
            this.snake});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(526, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // clear
            // 
            this.clear.Enabled = false;
            this.clear.Image = global::Game_of_Life.Properties.Resources.clear;
            this.clear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(54, 22);
            this.clear.Text = "Clear";
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // reset
            // 
            this.reset.Enabled = false;
            this.reset.Image = global::Game_of_Life.Properties.Resources.reset;
            this.reset.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.reset.Name = "reset";
            this.reset.Size = new System.Drawing.Size(55, 22);
            this.reset.Text = "Reset";
            this.reset.Click += new System.EventHandler(this.reset_Click);
            // 
            // randomize
            // 
            this.randomize.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.randomizeTime,
            this.randomizeCurrentSeed,
            this.randomizeNewSeed});
            this.randomize.Image = global::Game_of_Life.Properties.Resources.randomize;
            this.randomize.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.randomize.Name = "randomize";
            this.randomize.Size = new System.Drawing.Size(98, 22);
            this.randomize.Tag = "";
            this.randomize.Text = "Randomize";
            this.randomize.ButtonClick += new System.EventHandler(this.randomizeTime_Click);
            // 
            // randomizeTime
            // 
            this.randomizeTime.Name = "randomizeTime";
            this.randomizeTime.Size = new System.Drawing.Size(142, 22);
            this.randomizeTime.Text = "Time";
            this.randomizeTime.Click += new System.EventHandler(this.randomizeTime_Click);
            // 
            // randomizeCurrentSeed
            // 
            this.randomizeCurrentSeed.Name = "randomizeCurrentSeed";
            this.randomizeCurrentSeed.Size = new System.Drawing.Size(142, 22);
            this.randomizeCurrentSeed.Text = "Current Seed";
            this.randomizeCurrentSeed.Click += new System.EventHandler(this.randomizeCurrentSeed_Click);
            // 
            // randomizeNewSeed
            // 
            this.randomizeNewSeed.Name = "randomizeNewSeed";
            this.randomizeNewSeed.Size = new System.Drawing.Size(142, 22);
            this.randomizeNewSeed.Text = "New Seed";
            this.randomizeNewSeed.Click += new System.EventHandler(this.randomizeNewSeed_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // run
            // 
            this.run.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runTo});
            this.run.Enabled = false;
            this.run.Image = global::Game_of_Life.Properties.Resources.run;
            this.run.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.run.Name = "run";
            this.run.Size = new System.Drawing.Size(60, 22);
            this.run.Text = "Run";
            this.run.ButtonClick += new System.EventHandler(this.run_ButtonClick);
            // 
            // runTo
            // 
            this.runTo.Name = "runTo";
            this.runTo.Size = new System.Drawing.Size(120, 22);
            this.runTo.Text = "Run To...";
            this.runTo.Click += new System.EventHandler(this.runTo_Click);
            // 
            // pause
            // 
            this.pause.Enabled = false;
            this.pause.Image = global::Game_of_Life.Properties.Resources.pause;
            this.pause.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pause.Name = "pause";
            this.pause.Size = new System.Drawing.Size(58, 22);
            this.pause.Text = "Pause";
            this.pause.Click += new System.EventHandler(this.pause_Click);
            // 
            // next
            // 
            this.next.Enabled = false;
            this.next.Image = global::Game_of_Life.Properties.Resources.next;
            this.next.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(51, 22);
            this.next.Text = "Next";
            this.next.Click += new System.EventHandler(this.next_Click);
            // 
            // snake
            // 
            this.snake.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pauseGame,
            this.setSnakeColor,
            this.setPowerUpColor,
            this.resetDefaultColors,
            this.exitGame});
            this.snake.Image = global::Game_of_Life.Properties.Resources.play_snake;
            this.snake.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.snake.Name = "snake";
            this.snake.Size = new System.Drawing.Size(95, 22);
            this.snake.Text = "Play Snake";
            this.snake.ButtonClick += new System.EventHandler(this.snake_ButtonClick);
            this.snake.DropDownClosed += new System.EventHandler(this.snake_DropDownClosed);
            this.snake.DropDownOpened += new System.EventHandler(this.snake_DropDownOpened);
            // 
            // pauseGame
            // 
            this.pauseGame.Enabled = false;
            this.pauseGame.Image = global::Game_of_Life.Properties.Resources.pause_unpause;
            this.pauseGame.Name = "pauseGame";
            this.pauseGame.Size = new System.Drawing.Size(190, 22);
            this.pauseGame.Text = "Pause/Unpause Game";
            this.pauseGame.Click += new System.EventHandler(this.pauseGame_Click);
            // 
            // setSnakeColor
            // 
            this.setSnakeColor.BackColor = System.Drawing.SystemColors.Control;
            this.setSnakeColor.Image = global::Game_of_Life.Properties.Resources.color;
            this.setSnakeColor.Name = "setSnakeColor";
            this.setSnakeColor.Size = new System.Drawing.Size(190, 22);
            this.setSnakeColor.Text = "Set Snake Color";
            this.setSnakeColor.Click += new System.EventHandler(this.setSnakeColor_Click);
            // 
            // setPowerUpColor
            // 
            this.setPowerUpColor.Image = global::Game_of_Life.Properties.Resources.color;
            this.setPowerUpColor.Name = "setPowerUpColor";
            this.setPowerUpColor.Size = new System.Drawing.Size(190, 22);
            this.setPowerUpColor.Text = "Set Power-Up Color";
            this.setPowerUpColor.Click += new System.EventHandler(this.setPowerUpColor_Click);
            // 
            // resetDefaultColors
            // 
            this.resetDefaultColors.Image = global::Game_of_Life.Properties.Resources.color_reset;
            this.resetDefaultColors.Name = "resetDefaultColors";
            this.resetDefaultColors.Size = new System.Drawing.Size(190, 22);
            this.resetDefaultColors.Text = "Reset Default Colors";
            this.resetDefaultColors.Click += new System.EventHandler(this.resetDefaultColors_Click);
            // 
            // exitGame
            // 
            this.exitGame.Enabled = false;
            this.exitGame.Image = global::Game_of_Life.Properties.Resources.quit;
            this.exitGame.Name = "exitGame";
            this.exitGame.Size = new System.Drawing.Size(190, 22);
            this.exitGame.Text = "Exit Game";
            this.exitGame.Click += new System.EventHandler(this.quitGame_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.info});
            this.statusStrip1.Location = new System.Drawing.Point(0, 575);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(526, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // info
            // 
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(28, 17);
            this.info.Text = "Info";
            // 
            // graphicsPanel
            // 
            this.graphicsPanel.BackColor = System.Drawing.SystemColors.Window;
            this.graphicsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphicsPanel.Location = new System.Drawing.Point(0, 49);
            this.graphicsPanel.Name = "graphicsPanel";
            this.graphicsPanel.Size = new System.Drawing.Size(526, 526);
            this.graphicsPanel.TabIndex = 3;
            this.graphicsPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.graphicsPanel_Paint);
            this.graphicsPanel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.graphicsPanel_MouseClick);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "World Files (*.world)|*.world";
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "World Files (*.world)|*.world";
            // 
            // importFileDialog
            // 
            this.importFileDialog.Filter = "Plaintext Files (*.cells)|*.cells";
            // 
            // timer
            // 
            this.timer.Interval = 11;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(170, 26);
            // 
            // toolStripMenuItem
            // 
            this.toolStripMenuItem.Enabled = false;
            this.toolStripMenuItem.Name = "toolStripMenuItem";
            this.toolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.toolStripMenuItem.Text = "Generations Alive:";
            // 
            // graphicsPanelSnake
            // 
            this.graphicsPanelSnake.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphicsPanelSnake.Location = new System.Drawing.Point(0, 49);
            this.graphicsPanelSnake.Name = "graphicsPanelSnake";
            this.graphicsPanelSnake.Size = new System.Drawing.Size(526, 526);
            this.graphicsPanelSnake.TabIndex = 4;
            this.graphicsPanelSnake.Visible = false;
            this.graphicsPanelSnake.Paint += new System.Windows.Forms.PaintEventHandler(this.graphicsPanelSnake_Paint);
            // 
            // gameTimer
            // 
            this.gameTimer.Tick += new System.EventHandler(this.gameTimer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 597);
            this.Controls.Add(this.graphicsPanelSnake);
            this.Controls.Add(this.graphicsPanel);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Game of Life";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private GraphicsPanel graphicsPanel;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem open;
        private System.Windows.Forms.ToolStripMenuItem save;
        private System.Windows.Forms.ToolStripMenuItem import;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem quit;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem options;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem about;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grid;
        private System.Windows.Forms.ToolStripMenuItem neighborCount;
        private System.Windows.Forms.ToolStripSplitButton run;
        private System.Windows.Forms.ToolStripMenuItem runTo;
        private System.Windows.Forms.ToolStripButton pause;
        private System.Windows.Forms.ToolStripButton next;
        private System.Windows.Forms.ToolStripSplitButton randomize;
        private System.Windows.Forms.ToolStripMenuItem randomizeCurrentSeed;
        private System.Windows.Forms.ToolStripMenuItem randomizeNewSeed;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem randomizeTime;
        private System.Windows.Forms.ToolStripStatusLabel info;
        private System.Windows.Forms.ToolStripButton reset;
        private System.Windows.Forms.ToolStripButton clear;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.OpenFileDialog importFileDialog;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem;
        private GraphicsPanel graphicsPanelSnake;
        private System.Windows.Forms.Timer gameTimer;
        private System.Windows.Forms.ToolStripSplitButton snake;
        private System.Windows.Forms.ToolStripMenuItem setPowerUpColor;
        private System.Windows.Forms.ToolStripMenuItem exitGame;
        private System.Windows.Forms.ToolStripMenuItem setSnakeColor;
        private System.Windows.Forms.ToolStripMenuItem pauseGame;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.ToolStripMenuItem resetDefaultColors;
    }
}

