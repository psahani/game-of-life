﻿using System;
using System.Collections.Generic;

namespace Game_of_Life {
    class SnakeGame {
        public enum Direction { Up, Left, Down, Right };

        //A list of points stored using 2-D int arrays that represents the snake
        public List<int[]> snake { get; set; }
        public Direction dir { get; set; }
        public bool poweringUp { get; set; }
        public int score { get; set; }

        //The size of the world
        public int width { get; set; }
        public int height { get; set; }

        //Stores the location of the power up
        public int[] powerUp { get; set; }
        public bool gameOver { get; set; }

        public SnakeGame(int width, int height) {
            Random rand = new Random();

            this.snake = new List<int[]>();

            //Initializes the snake with a head at a random location
            int[] head = new int[2];
            head[0] = rand.Next(width);
            head[1] = rand.Next(height);
            snake.Add(head);

            this.dir = (Direction)(rand.Next(4));
            this.poweringUp = false;
            this.score = 10;

            this.width = width;
            this.height = height;

            this.powerUp = new int[2];
            this.gameOver = false;

            //Spawns the first power up at a random location
            spawnPowerUp();
        }

        private bool containedInSnake(int[] coordinate) {
            foreach (int[] s in snake) {
                //If the coordinate matches a coordinate found in the snake list, return true
                if (s[0] == coordinate[0] && s[1] == coordinate[1]) {
                    return true;
                }
            }

            return false;
        }

        private void spawnPowerUp() {
            Random rand = new Random();

            //Generate random power up locations until it's not at a spot where the snake already is
            do {
                powerUp[0] = rand.Next(width);
                powerUp[1] = rand.Next(height);
            } while (containedInSnake(powerUp));
        }

        public void moveSnake(Direction input, bool toroidal) {
            //Makes sure that the snake isn't trying to move backwards into itself
            if (((int)input + 2) % 4 != (int)dir) {
                dir = input;
            }

            int[] next = new int[2];
            Array.Copy(snake[0], next, 2);

            //Checks if the game world is set to toroidal or not in options
            if (toroidal) {
                switch (dir) {
                    case SnakeGame.Direction.Up:
                        next[1] = (snake[0][1] - 1 + height) % height;
                        break;
                    case SnakeGame.Direction.Down:
                        next[1] = (snake[0][1] + 1 + height) % height;
                        break;
                    case SnakeGame.Direction.Left:
                        next[0] = (snake[0][0] - 1 + width) % width;
                        break;
                    case SnakeGame.Direction.Right:
                        next[0] = (snake[0][0] + 1 + width) % width;
                        break;
                }
            } else {
                switch (dir) {
                    case SnakeGame.Direction.Up:
                        next[1] = snake[0][1] - 1;
                        break;
                    case SnakeGame.Direction.Down:
                        next[1] = snake[0][1] + 1;
                        break;
                    case SnakeGame.Direction.Left:
                        next[0] = snake[0][0] - 1;
                        break;
                    case SnakeGame.Direction.Right:
                        next[0] = snake[0][0] + 1;
                        break;
                }
            }

            //If the snake has collided into itself or a wall, end the game
            if (checkCollision(next, toroidal)) {
                gameOver = true;
                return;
            } else {
                //Moves the snake forward by 1 square
                snake.Insert(0, next);
            }

            //If the snake just got a power-up, don't delete the tail
            if (!poweringUp) {
                snake.RemoveAt(snake.Count - 1);
            } else {
                poweringUp = false;
            }
        }

        private bool checkCollision(int[] next, bool toroidal) {
            //Checks if the snake collided with a power-up or not
            if (next[0] == powerUp[0] && next[1] == powerUp[1]) {
                score += 10;
                poweringUp = true;

                spawnPowerUp();
            //Checks if the snake crashed into itself
            } else if (containedInSnake(next)) {
                return true;
            } else if (!toroidal) {
                //Checks if the snake crashed into a wall
                if (next[0] < 0 || next[0] >= width) {
                    return true;
                }

                if (next[1] < 0 || next[1] >= height) {
                    return true;
                }
            }

            return false;
        }
    }
}
